//Copyright (c) 2007 Sun Microsystems

//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation
//files (the "Software"), to deal in the Software without
//restriction, including without limitation the rights to use,
//copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the
//Software is furnished to do so, subject to the following
//conditions:

//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.i18ntask.test;

import net.java.hulp.i18n.LocalizationSupport;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

/**
 * To run this test: 
 * First execute "ant lib testi18ntask" in "hulp\i18n"
 * Make sure to add "hulp\i18n\build\hulp\classes3" to the classpath of this test
 * 
 * @author fkieviet
 */
public class TaskTest extends TestCase {
    public static final String s1b = "E001: Test1xxxxx\\xxxxxxxxxx\ttabnnewline\nnextline";
    public static final String s1c = "E131: Could not find files with pattern {1} in directory {0}: {2}";
    public static Pattern DEFAULTPATTERN = Pattern.compile("([A-Z]\\d\\d\\d)(: )(.*)", Pattern.DOTALL); 

    public void test() {
        String e002a = "E002: Test1";
        String e002b = "E002: Duplicate number";
        assertTrue(!e002a.equals(e002b));

        Pattern p = DEFAULTPATTERN;
        Matcher m = p.matcher(s1b);
        assertTrue(m.matches());

        String s = s1b.substring(0, 10);
        System.out.println(s);
        String s2 = Localizer.get().t(s);
        System.out.println("TEST-" + s1b);
        System.out.println(s2);
        assertTrue(("TEST-" + s1b).equals(s2));
    }

    public static class Localizer extends LocalizationSupport {
        public Localizer() {
            super("TEST-");
        }
        private static final Localizer s = new Localizer();
        public static Localizer get() {
            return s;
        }
    }

    public class X {
        Logger sLog = Logger.getLogger(X.class.getName());
        Localizer sLoc = Localizer.get();

        String dir = null;
        String pattern = null;
        Exception ex = null;

        public void test() {
            sLog.log(Level.WARNING, sLoc.t("E131: Could not find files with pattern {1} in directory {0}: {2}", dir, pattern, ex), ex);
        }
    }
}
