// Copyright (c) 2007 Sun Microsystems
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.i18n.test;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.LocalizedString;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import junit.framework.TestCase;

public class LocalizationTest extends TestCase {
    
    private static class Localizer extends LocalizationSupport {
        public Localizer() {
            super(null, "TEST-", null);
        }
        private static final Localizer s = new Localizer();
        public static Localizer get() {
            return s;
        }
    }
    
    public LocalizationTest() {
        
    }
    
    public LocalizationTest(String name) {
        super(name);
    }
    
    public void testMatch() throws Throwable {
        String msg = "X000: abcdef";
        Matcher matcher = LocalizationSupport.DEFAULTPATTERN.matcher(msg);
        boolean b = matcher.matches();
        assertTrue(b);
        
        int groups = matcher.groupCount();
        assertTrue(groups == 3);
        
        String g1 = matcher.group(1);
        String g2 = matcher.group(2);
        String g3 = matcher.group(3);
        assertTrue(g1.equals("X000"));
        assertTrue(g2.equals(": "));
        assertTrue(g3.equals("abcdef"));
        
        Localizer loc = new Localizer();
        LocalizedString s = loc.x("X000: abcde");
        String ss = s.toString();
        assertTrue(ss.equals("TEST-X000: ABCDEF"));
        
        s = loc.x("X001: XXX", "a", "b", "c");
        ss = s.toString();
        assertTrue("TEST-X001: Aa Bb C'c'".equals(ss));
        
        s = loc.xNoID("X001: XXX", "a", "b", "c");
        ss = s.toString();
        assertTrue("Aa Bb C'c'".equals(ss));
    }
    
    /**
     * Should fallback to US if locale not found
     * 
     * @throws Throwable on failure
     */
    public void testFallback() throws Throwable {
        // Should fallback to US if locale not found
        Locale def = Locale.getDefault();
        Locale.setDefault(Locale.KOREAN);
        Localizer loc = new Localizer();
        LocalizedString s = loc.x("X001: XXX", "a", "b", "c");
        String ss = s.toString();
        assertTrue("TEST-X001: Aa Bb C'c'".equals(ss));

        s = loc.xNoID("X001: XXX", "a", "b", "c");
        ss = s.toString();
        assertTrue("Aa Bb C'c'".equals(ss));
        
        Locale.setDefault(def);
    }
    
    /**
     * Should read foreign
     * 
     * @throws Throwable on failure
     */
    public void testForeign() throws Throwable {
        // 
        Locale def = Locale.getDefault();
        Locale.setDefault(new Locale("nl"));
        Localizer loc = new Localizer();
        LocalizedString s = loc.x("X001: XXX", "a", "b", "c");
        String ss = s.toString();
        assertTrue("TEST-X001: Dutch: Aa Bb C'c'".equals(ss));

        s = loc.xNoID("X001: XXX", "a", "b", "c");
        ss = s.toString();
        assertTrue("Dutch: Aa Bb C'c'".equals(ss));
        Locale.setDefault(def);
    }
    
    /**
     * Missing ID should work
     * 
     * @throws Throwable on failure
     */
    public void testNotfound() throws Throwable {
        Localizer loc = new Localizer();
        {
            LocalizedString s = loc.x("Y001: XXX", "a", "b", "c");
            String ss = s.toString();
            assertTrue("TEST-Y001: XXX".equals(ss));
        }
        {
            LocalizedString s = loc.xNoID("Y001: XXX", "a", "b", "c");
            String ss = s.toString();
            assertTrue("XXX [*]".equals(ss));
        }
        {
            LocalizedString s = loc.x("Y001: a={0}, b={1}", "a", "b", "c");
            String ss = s.toString();
            assertTrue("TEST-Y001: a=a, b=b".equals(ss));
        }
        {
            LocalizedString s = loc.xNoID("Y001: a={0}, b={1}", "a", "b", "c");
            String ss = s.toString();
            assertTrue("a=a, b=b [*]".equals(ss));
        }
    }
    
    /**
     * Invalid ID should work
     * 
     * @throws Throwable on failure
     */
    public void testInvalidID() throws Throwable {
        Localizer loc = new Localizer();
        {
            LocalizedString s = loc.x("Some text", "a", "b", "c");
            String ss = s.toString();
            assertTrue("TEST-<?>: Some text".equals(ss));
        }
        {
            LocalizedString s = loc.xNoID("Some text", "a", "b", "c");
            String ss = s.toString();
            assertTrue("Some text (*)".equals(ss));
        }
        {
            LocalizedString s = loc.x("Some test a={0}, b={1}", "a", "b", "c");
            String ss = s.toString();
            assertTrue("TEST-<?>: Some test a=a, b=b".equals(ss));
        }
        {
            LocalizedString s = loc.xNoID("Some test a={0}, b={1}", "a", "b", "c");
            String ss = s.toString();
            assertTrue("Some test a=a, b=b (*)".equals(ss));
        }
    }

    /**
     * Invalid Msg should work
     * 
     * @throws Throwable on failure
     */
    public void testInvalidMsg() throws Throwable {
        Localizer loc = new Localizer();
        String v = "Some{ text";
        {
            LocalizedString s = loc.x(v, "a123", "b123", "c123");
            String ss = s.toString();
            assertTrue(ss.indexOf(v) > 0);
            assertTrue(ss.indexOf("a123") > 0);
            assertTrue(ss.indexOf("b123") > 0);
            assertTrue(ss.indexOf("c123") > 0);
        }
        {
            LocalizedString s = loc.xNoID(v, "a123", "b123", "c123");
            String ss = s.toString();
            assertTrue(ss.indexOf(v) == 0);
            assertTrue(ss.indexOf("a123") > 0);
            assertTrue(ss.indexOf("b123") > 0);
            assertTrue(ss.indexOf("c123") > 0);
        }
    }
}
