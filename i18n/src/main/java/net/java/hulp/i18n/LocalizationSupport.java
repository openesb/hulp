// Copyright (c) 2007 Sun Microsystems
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.i18n;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tools for obtaining localized messages
 *
 * @author fkieviet
 * @version $Revision: 1.6 $
 */
public abstract class LocalizationSupport {
    private PropertyResourceBundle mBundle;
    private Pattern mIdPattern;
    private String mPrefix;
    private String mBundleName;
    
    /**
     * Default name for resource bundles
     */
    public static final String DEFAULTBUNDLENAME = "msgs";

    /**
     * Default pattern to parse a message with
     */
    public static Pattern DEFAULTPATTERN = Pattern.compile("([A-Z]\\d\\d\\d)(: )(.*)", Pattern.DOTALL); 
    
    /**
     * @param idpattern pattern to parse message
     * @param prefix module name
     */
    protected LocalizationSupport(Pattern idpattern, String prefix, String bundlename) {
        mIdPattern = idpattern == null ? DEFAULTPATTERN : idpattern;
        mPrefix = prefix;
        mBundleName = bundlename == null ? DEFAULTBUNDLENAME : bundlename;
        // Strip off the class to obtain the package name
        String packagename = this.getClass().getName();
        int lastdot = packagename.lastIndexOf(".");
        packagename = packagename.substring(0, lastdot);
        try {
            mBundle = (PropertyResourceBundle) ResourceBundle.getBundle(packagename + "." + mBundleName, 
                Locale.getDefault(), getClass().getClassLoader());
        } catch (Exception e) {
            throw new RuntimeException("Resource bundle could not be loaded: " + e, e);
        }
        
        // Classname should be Localizer so that later the Ant task can be extended to 
        // automatically detect packages and resource bundles
        String clname = this.getClass().getName();
        if (!clname.endsWith(".Localizer") && !clname.endsWith("$Localizer")) {
            throw new RuntimeException("Localizer class [" + clname + "] should be [Localizer]");
        }
    }
    
    /**
     * @param idpattern pattern to parse message
     * @param prefix module name
     */
    protected LocalizationSupport(String prefix) {
        this(null, prefix, null);
    }

    private String salvage(Object[] args) {
        StringBuffer ret = new StringBuffer();
        ret.append(" [FORMAT ERROR.");
        if (args != null && args.length > 0) {
            ret.append(" Arguments: ");
        }
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                ret.append(", ");
            }
            ret.append("{").append(i).append("}=\"").append(args[i]).append("\"");
        }
        ret.append("]");
        return ret.toString();
    }
    
    private String format(String msg, Object[] args) {
        try {
            return MessageFormat.format(msg, args);
        } catch (Exception e) {
            // Format error, e.g. "my msg{{0}/{1}", return "my msg{{0}/{1} [invalid format, {0}=xx, {1}=yy]"
            return msg + salvage(args);
        }
    }
    
    /**
     * @see #t(String, Object...)
     * 
     * @param msg Message to be localized
     * @param args arguments
     * @return LocalizedString
     */
    public LocalizedString x(String msg, Object... args) {
        return new LocalizedString(t(msg, args));
    }
    
    /**
     * Returns a localized message.
     * 
     * Msg is a string of the form "E001: this is a message". The format of the id is 
     * specified in the constructor, and is the first group of the pattern. E.g. in a 
     * specified pattern of "([A-Z]\d\d\d)(: )( .*), the message id is [A-Z]\d\d\d; in 
     * the example that would be E001.
     * 
     * It returns the localized form of "MODULE-E999: This is a message", e.g. 
     * "MODULE-X001: Dit is een bericht".
     * 
     * If the message is improperly formatted, it will return MODULE-<?>: This is a message
     * If the message cannot be found, it will return "MODULE-E999: This is a message" (English)
     * 
     * @param msg Message to be localized
     * @param args arguments
     * @return java.lang.String
     */
    public String t(String msg, Object... args) {
        try {
            Matcher matcher = mIdPattern.matcher(msg);
            if (!matcher.matches() || matcher.groupCount() <= 1) {
                // Improperly formatted string: no ID.
                // e.g. "This is a message", return "MODULE-<?>: This is a message" 
                return mPrefix + "<?>: " + format(msg, args);
            } else {
                // Properly formatted message, e.g. "X001: This is a message"; get "X001"
                String msgid = matcher.group(1);
                
                // Load string
                String localizedmsg;
                try {
                    localizedmsg = mBundle.getString(mPrefix + msgid);
                } catch (Exception e) {
                    // load error, return "MODULE-E999: This is a message" (English)
                    return mPrefix + format(msg, args);
                }

                // Return localized "MODULE-X001: Dit is een bericht" (Localized)
                return mPrefix + msgid + ": " + format(localizedmsg, args);
            } 
        } catch (Exception e) {
            return mPrefix + format(msg, args);
        }
    }
    
    /**
     * Returns a localized message without its messageid.
     * 
     * Msg is a string of the form "E001: this is a message". The format of the id is 
     * specified in the constructor, and is the first group of the pattern. E.g. in a 
     * specified pattern of "([A-Z]\d\d\d)(: )( .*), the message id is [A-Z]\d\d\d; in 
     * the example that would be E001.
     * 
     * It returns the localized form of "This is a message", e.g. "Dit is een bericht".
     * 
     * If the message is improperly formatted, e.g. "INV: This is a message" will return 
     * "INV: This is a message (*)"
     * 
     * If the message cannot be found in the bundle, it will return "This is a message"
     * provided that the regular expression has two groups. If the regurlar expression
     * yields only one group, it will return  "X001: this is a message".
     * 
     * @param msg msg to localize in the form of "X001: this is a message" 
     *   (the exact format must match the specified regular expression)
     * @param args arguments to substitute in msg
     * @return localized string
     */
    public String tNoID(String msg, Object... args) {
        try {
            Matcher matcher = mIdPattern.matcher(msg);
            if (!matcher.matches() || matcher.groupCount() <= 1) {
                // Improperly formatted string: no ID or invalid groups.
                // e.g. "INV: This is a message", return "INV: This is a message (*)" 
                return format(msg, args) + " (*)";
            } else {
                // Properly formatted message, e.g. "X001: This is a message"; get "X001"
                String msgid = matcher.group(1);
                
                // Load string
                String localizedmsg;
                try {
                    localizedmsg = mBundle.getString(mPrefix + msgid);
                } catch (Exception e) {
                    // load error, try to return "This is a message [*]" (English)
                    if (matcher.groupCount() == 1) {
                        // Invalid groups: return "X001: This is a message [*]"
                        return format(msg, args) + " [*]";
                    } else {
                        // Return "This is a message [*]"
                        String msgOnly = matcher.group(matcher.groupCount());
                        return format(msgOnly, args) + " [*]";
                    }
                }

                // Return localized "Dit is een bericht" (Localized)
                return format(localizedmsg, args);
            } 
        } catch (Exception e) {
            return format(msg, args);
        }
    }
    
    /**
     * @see #tNoID(String, Object...)
     * 
     * @param msg Message to be localized
     * @param args arguments
     * @return LocalizedString
     */
    public LocalizedString xNoID(String msg, Object... args) {
        return new LocalizedString(tNoID(msg, args));
    }
    
    
}
