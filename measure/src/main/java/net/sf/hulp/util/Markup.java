// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.util;

/**
 * Provides formatting tags for different output media, e.g. plain text
 * and HTML.
 */
public class Markup {
    protected static final String LF = System.getProperty("line.separator");
    int mSpan;

    public static final int TABLESTYLE_SIMPLE = 0;
    public static final int TABLESTYLE_BANNER = 1;
    public static final int TABLESTYLE_BANNEREGAL = 2;
    public static final int TABLESTYLE_BARE = 3;
    
    private String repeat(String what, int howmany) {
        if (howmany < 0) {
            return "";
        }
        StringBuffer ret = new StringBuffer(what.length() * howmany);
        for (int i = 0; i < howmany; i++) {
            ret.append(what);
        }
        return ret.toString();
    }

    public String beginPage() {
        return "";
    }
    public String endPage() {
        return "";
    }
    public String beginTable(int style) {
        return "";
    }
    public String endTable() {
        return LF;
    }
    public String beginRow() {
        return "";
    }
    public String beginRow(int span) {
        mSpan = span;
        return "";
    }
    public String beginHRow() {
        return "";
    }
    public String beginHRow(int span) {
        mSpan = span;
        return "";
    }
    public String endRow() {
        String ret = repeat("\t", mSpan - 1) + LF;
        mSpan = 0;
        return ret;
    }
    public String tab() {
        String ret = repeat("\t", mSpan - 1) + "\t";
        mSpan = 0;
        return ret;
    }
    public String tab(int span) {
        String ret = repeat("\t", mSpan - 1) + "\t";
        mSpan = span;
        return ret;
    }
    public String br() {
        return LF;
    }
    public String beginArea() {
        return "";
    }
    public String endArea() {
        return "";
    }
    public String caption(String str) {
        return str;
    }
    public String bold(String str) {
        return str;
    }
    public String strong(String str) {
        return str;
    }
    public String fine(String str) {
        return str;
    }
    public String makeSafe(String val) {
        return val;
    }
    public String beginPre() {
        return "";
    }
    public String endPre() {
        return "";
    }
}