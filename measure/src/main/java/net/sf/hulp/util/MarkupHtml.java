// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.util;

/**
 * Specialized for HTML markup
 */
public class MarkupHtml extends Markup {
    final static private int MAXTABLES = 20;
    private Table[] mTableStack = new Table[MAXTABLES];
    private int m_iTable = -1;

    /**
     * Displays a horizontal menu
     */
    public static class Menu {
        StringBuffer m_data = new StringBuffer(200);
        boolean      m_added;

        public Menu(String name) {
            String top = "<html>\n"
                       + "\n"
                       + "<head>\n"
                       + "<base target=\"body\">\n"
                       + "<title>" +name+ "</title>\n"
                       + "<STYLE TYPE=\"text/css\">\n"
                       + ".hd1{font:bold 14px verdana;color:#787878}\n"
                       + ".mnu1{color:#FFFFFF;text-decoration:none;font:bold 12px verdana}\n"
                       + ".mnu2{color:#000000;text-decoration:none;font:bold 12px verdana}\n"
                       + ".pd1{padding-left:15px}\n"
                       + "A:link{color:#003399}\n"
                       + "A:visited{color:#800080}\n"
                       + "A:hover{color:#FF3300}\n"
                       + "TD{font-family: Verdana,Arial,Helvetica; font-size: xx-small;}\n"
                       + "</STYLE>\n"
                       + "\n"
                       + "</head>\n"
                       + "\n"
                       + "<body bgcolor=\"#6699CC\" topmargin=\"0\" leftmargin=\"15\" link=\"#FFFFFF\" vlink=\"#FFFFFF\" text=\"#FFFFFF\">\n"
                       + "<BASEFONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"3\">\n"
                       + "\n"
                       + "<a class=\"mnu2\">" +name+ ":&nbsp;&nbsp; </a>\n"
                       + "";
            m_data.append(top);

//            <html>
//
//            <head>
//            <base target="body">
//            <title>$(name)$</title>
//            <STYLE TYPE="text/css">
//            .hd1{font:bold 14px verdana;color:#787878}
//            .mnu1{color:#FFFFFF;text-decoration:none;font:bold 12px verdana}
//            .mnu2{color:#000000;text-decoration:none;font:bold 12px verdana}
//            .pd1{padding-left:15px}
//            A:link{color:#003399}
//            A:visited{color:#800080}
//            A:hover{color:#FF3300}
//            TD{font-family: Verdana,Arial,Helvetica; font-size: xx-small;}
//            </STYLE>
//
//            </head>
//
//            <body bgcolor="#6699CC" topmargin="0" leftmargin="15" link="#FFFFFF" vlink="#FFFFFF" text="#FFFFFF">
//            <BASEFONT FACE="Verdana, Arial, Helvetica" SIZE="3">
//
//            <a class="mnu2">$(name)$:&nbsp;&nbsp; </a>
//
//            <A CLASS="mnu1"
//            STYLE="color:#FFFFFF;"
//            HREF="$(link)$"
//            TARGET="body"
//            onmouseover="this.style.color='#FFCC00';"
//            onmouseout="this.style.color='#FFFFFF';">$(name)$</A>
//
//            &nbsp;&nbsp;<FONT COLOR='#FFFFFF'>|</FONT>&nbsp;&nbsp;
//
//
//            </body>
//            </html>
        }

        public Menu add(String link, String name) {
            if (m_added) {
                m_data.append("&nbsp;&nbsp;<FONT COLOR='#FFFFFF'>|</FONT>&nbsp;&nbsp;\n");
            } else {
                m_added = true;
            }

            String item =
                    "<A CLASS=\"mnu1\" \n"
                    + "STYLE=\"color:#FFFFFF;\" \n"
                    + "HREF=\"" +link+ "\" \n"
                    + "TARGET=\"body\" \n"
                    + "onmouseover=\"this.style.color='#FFCC00';\" \n"
                    + "onmouseout=\"this.style.color='#FFFFFF';\">" +name+ "</A>\n"
                    + "";

            m_data.append(item);

            return this;
        }

        public String toString() {
            return m_data + "</span></font></body></html>";
        }
    }

    static int hideRandom = 0;

    public String beginHide(String link) {
        // Thread safe not an issue
        return beginHide(link, "showhideid" + Integer.toString(++hideRandom));
    }

    public String beginHide(String link, String id) {
        return "<a href=\"javascript:nothing()\" onclick=\"javascript:showit('" +id+ "')\">" +link+ "</a>\n"
                + "<div ID=\"" +id+ "\" style=\"display: 'none'\">\n";
    }

    public String endHide() {
        return "</div>";
    }

    /**
     * Baseclass for all HTML tables
     */
    private static abstract class Table {
        abstract String beginTable();
        abstract String beginRow();
        abstract String beginRow(int span);
        abstract String beginHRow();
        abstract String beginHRow(int span);
        abstract String tab();
        abstract String tab(int span);
        String endRow() {
            return "</TT></TD></TR>\r\n";
        }
        String endTable() {
            return "</Table>";
        }
    }

    public String beginTable(int style) {
        switch (style) {
            case TABLESTYLE_SIMPLE :
                mTableStack[++m_iTable] = new SimpleTable();
                break;
            case TABLESTYLE_BANNER :
                mTableStack[++m_iTable] = new BannerTableEgal();
                break;
            case TABLESTYLE_BANNEREGAL :
                mTableStack[++m_iTable] = new BannerTableEgal();
                break;
            case TABLESTYLE_BARE :
                mTableStack[++m_iTable] = new TableBare();
                break;
            default:
                mTableStack[++m_iTable] = new SimpleTable();
                break;
        }

        return mTableStack[m_iTable].beginTable();
    }

    public String endTable() {
        return mTableStack[m_iTable--].endTable();
    }

    public String beginRow() {
        return mTableStack[m_iTable].beginRow();
    }

    public String beginRow(int span) {
        return mTableStack[m_iTable].beginRow(span);
    }

    public String beginHRow() {
        return mTableStack[m_iTable].beginHRow();
    }

    public String beginHRow(int span) {
        return mTableStack[m_iTable].beginHRow(span);
    }

    public String endRow() {
        return mTableStack[m_iTable].endRow();
    }

    public String tab() {
        return mTableStack[m_iTable].tab();
    }

    public String tab(int span) {
        return mTableStack[m_iTable].tab(span);
    }

    public String br() {
        return "<BR>\r\n";
    }

    public String beginArea() {
        return "<Table Border=1 CELLSPACING=0 bordercolor=#6699CC bordercolordark=#6699CC "
                + "bordercolorlight=#6699CC BGCOLOR=#6699CC><TR><TD>\r\n";
    }

    public String endArea() {
        return "</TD></TR></Table>\r\n";
    }

    public String caption(String str) {
        return "<br><font size=+1><b>" + str + "</b></font><br>";
    }

    public String bold(String str) {
        return "<b>" + str + "</b>";
    }

    public String strong(String str) {
        return "<font color=red><b>" + str + "</b></font>";
    }

    public String fine(String str) {
        return "<small>" + str + "</small>";
    }

    public String beginPage() {
        return "<html><head>\n"
                + "<SCRIPT LANGUAGE=\"JavaScript\"><!--\n"
                + "function showit(w) {\n"
                + "  if ( document.all[w].style.display == 'none') document.all[w].style.display = \"\"\n"
                + "  else document.all[w].style.display = \"none\";\n"
                + "}\n"
                + "function nothing() {}\n"
                + "--></script>\n"
                + "<STYLE TYPE='text/css'>\n"
                + "A         { TEXT-DECORATION: none }\n"
                + "A:link    { color:maroon; font-style: normal; } \n"
                + "A:visited { color:maroon; font-style: normal; } \n"
                + "A:hover   { COLOR: blue; }\n"
                + ".tblh1    {color:#FFFFFF;text-decoration:none;font: 12px verdana background-color: #055F9D; color: #FFFFFF\"}\n"
                + ".tbrwlt   {color:#FFFFFF;text-decoration:none;font: 12px verdana}\n"
                + "\n"
                + "</STYLE>\n"
                + "</head><body>\n"
                + "<BASEFONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"2\">\n"
                + "";
    }

    public String endPage() {
        return "</body></html>";
    }

    public String makeSafe(String val) {
        return xmlNormalize(val);
    }

    public String beginPre() {
        return "<pre>";
    }

    public String endPre() {
        return "</pre>";
    }

    /**
     * A HTML doc that contains two sub frames: one called body with URL ?body
     * and one called menu with URL ?menu
     * 
     * @return HTML doc
     */
    public String pageFrame() {
        return "<HTML><HEAD></HEAD>"
                +"<FRAMESET ROWS='22,*' FRAMEBORDER=0 FRAMESPACING=0 BORDER=0>\r\n"
                +"<FRAME NAME='menu' SCROLLING='NO' NORESIZE TARGET='body' "
                +"SRC='?menu' "
                +"marginwidth='2' marginheight='2'>\r\n"
                +"<FRAME NAME='body' "
                +"SRC='?body'>\r\n"
                +"</FRAMESET>\r\n"
                +"</HTML>";
    }

    public String link(String url) {
        return "<A HREF='" + url + "'>";
    }

    public String link() {
        return "</A>";
    }


    /**
     * A simple HTML table using borders.
     */
    private static class SimpleTable extends Table {
        String beginTable() { return "<Table Border=1 CELLSPACING=0 bordercolor=#C0C0C0 bordercolordark=#FFFFFF bordercolorlight=#C0C0C0>\r\n"; }
        String beginRow()   { return "<TR VALIGN=\"TOP\"><TD><TT>"; }
        String beginRow(int span)  { return "<TR VALIGN=\"TOP\"><TD COLSPAN=\"" + Integer.toString(span) + "\"><TT>"; }
        String beginHRow()   { return beginRow(); }
        String beginHRow(int span)  { return beginHRow(span); }
        String tab()        { return "</TT></TD><TD><TT>"; }
        String tab(int span){ return "</TT></TD><TD COLSPAN=\"" + Integer.toString(span) + "\"><TT>"; }
    }

    /**
     * A nicely formatted table with borders, no header row, on a light gray background
     */
    private class BannerTableEgal extends Table {
        private boolean mHeaderRowDone = false;
        String beginTable() {
            return "<table border=\"1\" cellpadding=\"2\"  Border=1 CELLSPACING=0 bordercolor=#C0C0C0 bordercolordark=#FFFFFF bordercolorlight=#FFFFFF>\n";
        }
        String beginRow()   {
            if (!mHeaderRowDone) {
                return "  <tr valign=\"top\">\n"
                + "    <td style=\"background-color: #055F9D; color: #FFFFFF\" class=\"tblh1\" valign=\"top\">";
            } else {
                return "  <tr>\n"
                + "    <td style=\"color: #000000\" class=\"tbrwlt\" bgcolor=\"#E5EAED\" valign=\"top\">";
            }
        }
        String beginRow(int span)   {
            if (!mHeaderRowDone) {
                return "  <tr valign=\"top\">\n"
                + "    <td style=\"background-color: #055F9D; color: #FFFFFF\" class=\"tblh1\" valign=\"top\" COLSPAN=\"" + Integer.toString(span) + "\">";
            } else {
                return "  <tr>\n"
                + "    <td style=\"color: #000000\" class=\"tbrwlt\" bgcolor=\"#E5EAED\" valign=\"top\" COLSPAN=\"" + Integer.toString(span) + "\">";
            }
        }
        String beginHRow()   {
            mHeaderRowDone = false;
            return beginRow();
        }
        String beginHRow(int span)   {
            mHeaderRowDone = false;
            return beginRow(span);
        }
        String tab() {
            if (!mHeaderRowDone) {
                return "&nbsp; </td>\n"
                + "    <td style=\"background-color: #055F9D; color: #FFFFFF\" class=\"tblh1\" valign=\"top\">";
            } else {
                return "&nbsp; </td>\n"
                + "    <td style=\"color: #000000\" class=\"tbrwlt\" bgcolor=\"#E5EAED\" valign=\"top\">";
            }
        }
        String tab(int span) {
            if (!mHeaderRowDone) {
                return "&nbsp; </td>\n"
                + "    <td style=\"background-color: #055F9D; color: #FFFFFF\" class=\"tblh1\" valign=\"top\" COLSPAN=\"" + Integer.toString(span) + "\">";
            } else {
                return "&nbsp; </td>\n"
                + "    <td style=\"color: #000000\" class=\"tbrwlt\" bgcolor=\"#E5EAED\" valign=\"top\" COLSPAN=\"" + Integer.toString(span) + "\">";
            }
        }
        String endRow()     {
            mHeaderRowDone = true;
            return "</TD></TR>";
        }
    }

    /**
     * An unformatted table without borders, no background
     */
    private class TableBare extends Table {
        private boolean m_headerRowDone = false;
        String beginTable() {
            return "<Table Border=0 CELLSPACING=0 width='100%'>";
        }
        String beginRow()   {
            return m_headerRowDone
            ? "<TR VALIGN=\"TOP\"><TD>"
            : "<TR VALIGN=\"TOP\"><TD>";
        }
        String beginRow(int span)   {
            return m_headerRowDone
            ? "<TR VALIGN=\"TOP\"><TD COLSPAN=\"" + Integer.toString(span) + "\">"
            : "<TR VALIGN=\"TOP\"><TD COLSPAN=\"" + Integer.toString(span) + "\">";
        }
        String beginHRow()   {
            m_headerRowDone = false;
            return beginRow();
        }
        String beginHRow(int span)   {
            m_headerRowDone = false;
            return beginRow(span);
        }
        String tab()        {
            return m_headerRowDone
            ? "&nbsp; </TD><TD>"
            : "&nbsp; </TD><TD>";
        }
        String tab(int span)        {
            return m_headerRowDone
            ? "&nbsp; </TD><TD COLSPAN=\"" + Integer.toString(span) + "\">"
            : "&nbsp; </TD><TD COLSPAN=\"" + Integer.toString(span) + "\">";
        }
        String endRow()     {
            m_headerRowDone = true;
            return super.endRow();
        }
    }

    /**
     * Makes a string xml safe
     */
    public static String xmlNormalize(String s) {
        StringBuffer ret = new StringBuffer(s.length());
        String esc = null;
        int end = s.length();
        for (int i = 0; i < end; i++) {
            char ch = s.charAt(i);
            switch(ch) {
                    case '<' : {
                        esc = "&lt;";
                        break;
                    }
                    case '>' : {
                        esc = "&gt;";
                        break;
                    }
                    case '&' : {
                        esc = "&amp;";
                        break;
                    }
                    case '"' : {
                        esc = "&quot;";
                        break;
                    }
                    case '\'' : {
                        esc = "&apos;";
                        break;
                    }
                    // else, default append char
                    default: {
                            if (('\u0020' > ch) || (ch > '\u007E')) {
                                // Unprintable or non-ASCII: use XML numeric
                                // character reference.
                                esc = "&#x";
                                for (int n = 12; n >= 0; n -= 4) {
                                    esc += ("0123456789ABCDEF".charAt(
                                                (ch >> n) & 0xF));
                                }
                                esc += ";";
                            }
                        }
            }
            if (esc != null) {
                ret.append(esc);
                esc = null;
            } else {
                ret.append(ch);
            }
        }
        return ret.toString();
    }
}
