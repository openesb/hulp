// Copyright (c) 2007 Sun Microsystems
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.measure.internal;

import net.java.hulp.measure.Probe;

import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;

import java.lang.reflect.Method;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A factory that delegates to a factory in a different classloader
 * 
 * @author fkieviet
 */
public class DelegatorFactory implements FactoryV2 {
    private Object mDelegate;
    private Method mCreate;
    private Method mEnd;
    private Method mSetSubtopic; 
    private Method mSetTopic;
    private Method mGetData;
    private Method mClearData;

    private class M extends Probe {
        private Object mRealMeasurement;

        public M(Object delegate) {
            mRealMeasurement = delegate;
        }

        @Override public void end() {
            try {
                mEnd.invoke(mRealMeasurement);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override public void setSubtopic(String subTopic) {
            try {
                mSetSubtopic.invoke(mRealMeasurement, subTopic);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override public void setTopic(String topic) {
            try {
                mSetTopic.invoke(mRealMeasurement, topic);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public DelegatorFactory(Object delegate) throws Exception {
        mDelegate = delegate;

        // Find Factory method
        Class c = delegate.getClass();
        mCreate = c.getMethod("createV2", int.class, Class.class, String.class, String.class);
        mGetData = c.getMethod("getData", List.class);
        mClearData = c.getMethod("clearData", List.class);

        // Find Measurement methods
        c = c.getClassLoader().loadClass(Probe.class.getName());
        mEnd = c.getMethod("end");
        mSetSubtopic = c.getMethod("setSubtopic", String.class);
        mSetTopic = c.getMethod("setTopic", String.class);
    }

    /**
     * Creates a new wrapper around a concreate measurement from the parent class
     * loader
     * 
     * @see net.sf.hulp.measure.Measurement.Factory#create(java.lang.String, java.lang.String)
     */
    public Probe createV2(int level, Class source, String topic, String subtopic) {
        Object target;
        try {
            target = mCreate.invoke(mDelegate, level, source, topic, subtopic);
            return new M(target);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @see net.java.hulp.measure.FactoryV2#getData(java.util.List)
     */
    public TabularData getData(List<Pattern[]> criteria) {
        try {
            return (TabularDataSupport) mGetData.invoke(mDelegate, criteria);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @see net.java.hulp.measure.FactoryV2#getData(java.util.List)
     */
    public void clearData(List<Pattern[]> criteria) {
        try {
            mClearData.invoke(mDelegate, criteria);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
