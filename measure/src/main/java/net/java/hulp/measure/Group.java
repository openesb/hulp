// Copyright (c) 2007 Sun Microsystems
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.measure;

import javax.management.openmbean.TabularData;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Defines a group of measurements using regular expressions
 * 
 * @author fkieviet
 */
public class Group {
    private List<String[]> mCriteria = new ArrayList<String[]>();
    
    private Group() {
    }
    
    /**
     * @return a new Group
     */
    public static Group createGroup() {
        return new Group();
    }
    
    /**
     * Adds a new pattern to this group
     * 
     * @param name regular expression for source[.]topic.
     * @param regular expression for subtopic
     */
    public void addPattern(String name, String subtopic) {
        mCriteria.add(new String[] {null, name, subtopic});
    }
    
    /**
     * Adds a new pattern to this group
     * 
     * @param source regular expression for class name
     * @param name regular expression for topic.
     * @param regular expression for subtopic
     */
    public void addPattern(String source, String name, String subtopic) {
        mCriteria.add(new String[] {source, name, subtopic});
    }
    
    private List<Pattern[]> getCriteria() {
        List<Pattern[]> ret = new ArrayList<Pattern[]>();
        for (String[] m : mCriteria) {
            if (m.length != 3) {
                throw new RuntimeException("Invalid criteria");
            }
            ret.add(new Pattern[] {
                Pattern.compile(m[0] == null ? ".*" : m[0]), 
                Pattern.compile(m[1] == null ? ".*" : m[1]), 
                Pattern.compile(m[2] == null ? ".*" : m[2])
            });
        }
        return ret;
    }
    
    /**
     * @return data for the specified criteria or null if no results
     */
    public TabularData fetchData() {
        return Probe.getData(getCriteria());
    }
    
    /**
     * Clears all data represented by this group
     */
    public void clearData() {
        Probe.clearData(getCriteria());
    }
}
